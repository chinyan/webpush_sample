const express = require('express');
const cors = require("cors")
const webPush = require('web-push');
const bodyParser = require('body-parser');
const dotenv = require("dotenv")
dotenv.config();

const app = express();
app.use(cors());
app.use(bodyParser.json());
const port = process.env.PORT;

console.table({
  port:process.env.PORT,
  WEBPUSH_PUBLIC_KEY: process.env.WEBPUSH_PUBLIC_KEY,
  WEBPUSH_PRIVATE_KEY: process.env.WEBPUSH_PRIVATE_KEY
})

// Initialize web-push with your VAPID keys
const publicKey = process.env.WEBPUSH_PUBLIC_KEY;
const privateKey = process.env.WEBPUSH_PRIVATE_KEY;
webPush.setVapidDetails("mailto:chinyan@lifelinelab.io", publicKey, privateKey);

// Store subscriptions
const subscriptions = {};

// Handle subscription requests
app.post('/subscribe', (req, res) => {
  const { subscription } = req.body;
  const clientIP = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
  subscriptions[clientIP] = subscription
  res.status(201).json({});
});

// Send push notifications
app.post('/sendNotification', (req, res) => {
  const payload = req.body;
  const clientIP = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
  const subscription = subscriptions[clientIP]  
  webPush.sendNotification(subscription, JSON.stringify(payload))
      .catch((err) => console.error(err));
  

  res.status(201).json({"message":"Notification sent"});
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
