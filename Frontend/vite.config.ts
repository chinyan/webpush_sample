import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'
import {VitePWA } from 'vite-plugin-pwa'



// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    VitePWA({
      srcDir: 'src',
      registerType: 'autoUpdate',
      filename: 'sw.ts',
      strategies: 'injectManifest',
      injectRegister: 'auto',
       // static assets in the public folder
      includeAssets: [
        'react.svg','vite.svg'
      ],
      manifest: {
        name: 'vite-react-pwa-web-push',
        short_name: 'web-push',
        description: 'A React PWA Push Notification sample',
        theme_color:'#171717',
        background_color: "#f0e7db",
        display: "standalone",
        orientation:'portrait',
        start_url: '/',
        scope: "/",
        icons:[{ 
          src: '/react.svg',
          sizes: 'any', // Specify the sizes of the icon
          type: 'image/svg+xml', // Specify the type of the icon (optional)
          purpose:"any"
        }],
      },
      // workbox: {
      //   globPatterns: ['**/*.{ts,js,css,svg}', 'index.html'],
      // },
      devOptions: {
        enabled: true,
        type: 'module',
      },
    }),
  ],
})
