import {ServiceWorkerTopic} from "./enum/serviceworker.enum"


// eslint-disable-next-line @typescript-eslint/no-unused-vars
self.addEventListener("activate", async (_e:Event) => {
    console.log("service worker activated")
})


// eslint-disable-next-line @typescript-eslint/no-explicit-any
self.addEventListener("push", (event:Event) => {    
    const e = event as PushEvent
    if(e.data){
        const payload =  JSON.parse(e.data.text()) 
        const title = payload.title || "untitled"
        const context = payload.body || "no context"
        e.waitUntil(
            self.registration.showNotification(title, { 
                body: context, 
                icon: "/react.svg",
                image:"/vite.svg",

            })
        );
        self.clients.matchAll().then((clients) => {
            clients.forEach((client) => {
                const payload = {
                    topic:ServiceWorkerTopic.NOTIFICATION,//"NOTIFICATION",
                    message:{
                        title,
                        body:context,
                    }
                }
                console.log("posting Message : ",payload)
                client.postMessage(payload);
            });
        });
    }else{
        console.log({e})
    }
})

