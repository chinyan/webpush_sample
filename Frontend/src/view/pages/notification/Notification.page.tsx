import { FC, useContext } from "react"
import { StoreContext } from "../../../stores/store.provider"
import "./NotificationPage.css"

export const NotificationPage:FC = () => {
    const {notifications} = useContext(StoreContext)!.notifications
    return (
        <div>
            <header className="page-header">
                <h1>Notification</h1> 
            </header>
            <div>
                <table className="notification-table">
                    <thead>
                        <tr>
                            <th>title</th>
                            <th>context</th>
                        </tr>
                    </thead>
                    <tbody>
                        {notifications.map(({title,body},id)=>(
                            <tr key={String(id)}>
                                <td>{title}</td>
                                <td>{body}</td>
                            </tr>
                        ))}
                    </tbody>
                    
                </table>
            </div>    
        </div>
    )
}