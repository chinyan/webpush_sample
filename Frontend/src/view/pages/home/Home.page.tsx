import {FC, ChangeEvent,useEffect, useRef } from "react"
import { createNotificationService } from "../../../services/app.services"
import { useAppHook } from "../../../hooks/app.hook"
import { InputRef } from "../../../entity/app.entity"
import "./HomePage.css"

export const HomePage:FC = () => {
    const {checkPermission,requestNotificationPermission,inputOnchangeHandler} = useAppHook()
    const inputRef = useRef<InputRef>({title:"",body:""})
    useEffect(()=>{
        (async () => {
        checkPermission();
        await requestNotificationPermission()
        })()
    },[checkPermission,requestNotificationPermission])

    return (
        <div className="home-page-container">
            <header className="App-header">
                <h1>Push Notification Service</h1>
            </header>
            <div className="input-container">
                <label>Title:</label> <input type="text" onChange={(e:ChangeEvent<HTMLInputElement>)=>{inputOnchangeHandler(e,inputRef,"title")}}/> 
            </div>
            <div className="input-container">
                <label>Context:</label> <input type="text" onChange={(e:ChangeEvent<HTMLInputElement>)=>{inputOnchangeHandler(e,inputRef,"body")}}/> 
            </div>
         
            <button onClick={async()=>{ await createNotificationService(inputRef.current)}}>send notification</button>
            
        </div>
    
    )
}