import {Link} from "react-router-dom"
import {FC} from "react"
import "./NavBar.css"

export const NavigationBar:FC = () => {
    return (
        <nav className="nav-menu">
            <ul>
                <li className="left-link"><Link to="/">Home</Link></li>
                <li className="right-link"><Link to="/notification">Notification</Link></li>
            </ul>
        </nav>
    )
}
