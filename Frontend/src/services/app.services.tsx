import { post } from "../helper/request"
import { InputRef } from "../entity/app.entity"


export const createNotificationService = async(payload:InputRef)=> {
        const url = `${import.meta.env.VITE_BASE_URL}/sendNotification`
        const {res,err} = await post(url,payload)
        if(err) alert(JSON.stringify(err))
        return {res}
}

export const saveSubscription = async (subscription:PushSubscription) => {
    console.log("sending ... ")
    const response = await fetch(`${import.meta.env.VITE_BASE_URL}/subscribe`, {
        method: 'post',
        headers: { 'Content-type': "application/json" },
        body: JSON.stringify({subscription})
    })

    return response.json()
}