import { ReactNode , FC, createContext, useState } from "react";
import { NotificationEntity,NotificationCtxEntity } from "../entity/notification.entity";


export const StoreContext = createContext<{
    notifications: NotificationCtxEntity
}|undefined>(undefined);

export const StoreProvider:FC<{children:ReactNode }> = ({children}) => {
    const [notifications,setNotifications] = useState<NotificationEntity[]>([])

    return (
        <StoreContext.Provider value={{
            notifications: {notifications,setNotifications}
        }}>
            {children}
        </StoreContext.Provider>
    )
}