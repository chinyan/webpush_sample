import { ChangeEvent, MutableRefObject } from "react";
import { InputRef } from "../entity/app.entity";

export const useAppHook = () => {
    const checkPermission = () => {
        if (!('serviceWorker' in navigator)) {
            throw new Error("No support for service worker!")
        }
  
        if (!('Notification' in window)) {
            throw new Error("No support for notification API");
        }
  
        if (!('PushManager' in window)) {
            throw new Error("No support for Push API")
        }
    }
  
    const requestNotificationPermission = async () => {
        const permission = await Notification.requestPermission();
  
        if (permission !== 'granted') {
            throw new Error("Notification permission not granted")
        }
    }

    const inputOnchangeHandler = (e: ChangeEvent<HTMLInputElement>, inputRef:MutableRefObject<InputRef>, key: keyof InputRef ) => {
        inputRef.current[key] = e.target.value;
       // console.log(inputRef); // Optional: Log the updated inputRef object
    };

    return {checkPermission,requestNotificationPermission,inputOnchangeHandler}

}