// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const post = async (url:string,payload:any) => {
    try{
        const res = await fetch(url,{
            method: 'post',
            headers: { 'Content-type': "application/json" },
            body: JSON.stringify(payload)
        });

        return {res:res.json()}
    }catch(err){
        return {err}
    }
}