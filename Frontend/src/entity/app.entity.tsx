export interface InputRef {
    title: string;
    body: string;
  }