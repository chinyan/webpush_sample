import { Dispatch, SetStateAction } from "react"

export interface NotificationEntity {
    title:string,
    body: string
}

export interface NotificationCtxEntity {
    notifications: Array<NotificationEntity>,
    setNotifications:Dispatch<SetStateAction<Array<NotificationEntity>>>
}