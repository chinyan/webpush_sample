import {FC, useContext, useEffect, useCallback, useRef } from "react"
import { Outlet } from "react-router-dom"
import { StoreContext } from "../stores/store.provider"
import { NavigationBar } from "../view/components/shared/Navbar"
import { ServiceWorkerTopic } from "../enum/serviceworker.enum"
import { urlBase64ToUint8Array } from "../helper/cryptography"
import { saveSubscription } from "../services/app.services"

export const Middleware:FC = () => {
    const {setNotifications} = useContext(StoreContext)!.notifications
    const isMountedRef = useRef(false);

    const subscribeNotification = useCallback(async(subscription:PushSubscription)=>await saveSubscription(subscription),[])

    const getSubscriptionData = useCallback(async()=>{
        const registration = await navigator.serviceWorker.getRegistration()
        const subscription = await registration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: urlBase64ToUint8Array(import.meta.env.VITE_WEBPUSH_KEY)
        })
        return subscription
    },[])
    const handleMessage = useCallback((event:MessageEvent)=>{
        // // Handle the received data, e.g., update React state
       const { topic,message } = event.data;
       switch (topic){
            case ServiceWorkerTopic.NOTIFICATION:{
                const {title,body} = message
                if(title&&body){
                    setNotifications(prev=>([...prev,{title,body}]))
                }
            }
            break;
            default:{console.log("unhandled topic : ", {topic,message})}
        }
   },[setNotifications]);

   //prompt and init notification subscription
   useEffect(()=>{
    if(!isMountedRef.current){ //handle use strict rerendering
        isMountedRef.current = true;
        (async()=>{
            if ('serviceWorker' in navigator && 'PushManager' in window) {
                const subscription = await getSubscriptionData()
                const response = await subscribeNotification(subscription)
                console.log({subscription,response})
            }else{ console.error("serviceWorker or PushManager is not supported in your browser")}
        })()
    }
   },[subscribeNotification,getSubscriptionData])

   useEffect(() => {
    (async()=>{
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.addEventListener('message', handleMessage);
            return () => {window.removeEventListener('message', handleMessage);};//clean up event listener
        } else { console.warn('Service Worker not supported in this browser'); }
    })()
  }, [handleMessage]);

    return (
        <div>
            <NavigationBar/>
            <div className="App">
                <Outlet/>
            </div>
        </div>
    )
}