import './App.css'
import {  FC } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { StoreProvider } from './stores/store.provider';
import { Middleware } from './middleware/middleware';
import { HomePage } from './view/pages/home/Home.page';
import { NotificationPage } from './view/pages/notification/Notification.page';

const router = createBrowserRouter([
  {
    path:"/",
    element: <Middleware/>,
    children:[
      {
        path:"/",
        element:<HomePage/>
      },{
        path:"/notification",
        element:<NotificationPage/>
      }
    ]
  }
])
const App:FC = () => {

  return (
    <div>
      <StoreProvider>
        <RouterProvider router={router}/>
      </StoreProvider>
    </div>
  );
}

export default App
